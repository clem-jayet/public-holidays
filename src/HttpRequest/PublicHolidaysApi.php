<?php

namespace App\HttpRequest;

use App\DTO\SettingsDTO;
use App\Exception\ApiPublicHolidaysException;
use App\Interface\PublicHolidaysObtainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class PublicHolidaysApi implements PublicHolidaysObtainerInterface
{
    private const BASE_ENDPOINT = 'https://calendrier.api.gouv.fr/jours-feries';

    public function __construct(private HttpClientInterface $httpClient)
    {
    }

    /**
     * @return array<string, string>
     */
    public function retrieve(SettingsDTO $settingsDTO): array
    {
        try {
            $url = sprintf('%s/%s/%d.json', self::BASE_ENDPOINT, $settingsDTO->zone, $settingsDTO->year);
            $response = $this->httpClient->request(Request::METHOD_GET, $url);

            return $response->toArray();
        } catch (\Exception $e) {
            throw new ApiPublicHolidaysException($e->getMessage());
        }
    }
}
