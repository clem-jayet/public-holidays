<?php

namespace App\Interface;

use App\DTO\SettingsDTO;

interface PublicHolidaysObtainerInterface
{
    /**
     * @return array<string, string>
     */
    public function retrieve(SettingsDTO $settingsDTO): array;
}
