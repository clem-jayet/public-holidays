<h1 align="center">Public Holidays</h1>
<h4 align="center">C'est quand le prochain jour férié en France ? </h4>

![screenshot](https://iili.io/JvLSnaV.png)

## C'est quoi ?

L'application utilise le framework Symfony pour afficher par défaut les jours fériés de l'année en cours pour la France métropolitaine. 

Un volet de paramétrages permet de choisir les 3 années à venir, ainsi que la zone, les jours fériés pouvant être différents pour la France métropolitaine, l'Alsace-Lorraine, ou encore l'Ile de la Réunion.

Les données sont obtenues à partir de l'API publique disponible sur https://api.gouv.fr/documentation/jours-feries.

Démo : https://jours-feries.smac-studio.fr/

# Installation  

## Pré-requis pour lancer en local

Avoir [Symfony CLI](https://symfony.com/download) installé sur votre ordinateur.

## Mode développement

Par défaut, l'application est configurée pour fonctionner en environnement de développement.

- Installation des dépendances :  ```composer install```
- Lancement du serveur local : ```symfony server:start```

## Mode production

- Créer le fichier `.env.local` à la racine de l'application, et et indiquer l'environnement production de la manière suivante  `APP_ENV=prod`
- Installation des dépendances :  ```composer install```
- Compiler les assets : ``php bin/console asset-map:compile``
- Lancement du serveur local : ```symfony server:start```
  
## Badges 

**php** : ![PHP coverage](https://gitlab.com/clem-jayet/public-holidays/badges/master/coverage.svg)

## Licence

[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)](https://opensource.org/licenses/)

## Credits  

Clément JAYET-LAVIOLETTE : 
[![LinkedIn Badge](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/cl%C3%A9ment-jayet-076754a2/)
