<?php

namespace App\Enum;

enum Zone: string
{
    public const DEFAULT = self::Metropole;

    case AlsaceMoselle = 'alsace-moselle';
    case Guadeloupe = 'guadeloupe';
    case Guyane = 'guyane';
    case LaReunion = 'la-reunion';
    case Martinique = 'martinique';
    case Mayotte = 'mayotte';
    case Metropole = 'metropole';
    case NouvelleCaledonie = 'nouvelle-caledonie';
    case PolynesieFrancaise = 'polynesie-francaise';
    case SaintBarthelemy = 'saint-barthelemy';
    case SaintMartin = 'saint-martin';
    case SaintPierreEtMiquelon = 'saint-pierre-et-miquelon';
    case WallisEtFutuna = 'wallis-et-futuna';

    /** @return array<int, string> */
    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }
}
