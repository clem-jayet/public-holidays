<?php

namespace App\Tests\Feature\Twig\Components;

use App\DTO\DateDTO;
use App\Twig\Components\OtherPublicHolidays;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\UX\LiveComponent\Test\InteractsWithLiveComponents;

class OtherPublicHolidaysTest extends KernelTestCase
{
    use InteractsWithLiveComponents;

    public const MAIN_HTML_NODE = '#other-public-holidays';

    public function testOtherPublicHolidaysCanBeEmpty(): void
    {
        $liveComponent = $this->createLiveComponent(
            name: OtherPublicHolidays::class,
            data: [],
        );

        $this->assertInstanceOf(OtherPublicHolidays::class, $liveComponent->component());
        $component = $liveComponent->component();
        $this->assertEmpty($component->otherPublicHolidays);
        $this->assertEmpty($liveComponent->render()->crawler()->filter(self::MAIN_HTML_NODE));
    }

    public function testOtherPublicHolidaysWithData(): void
    {
        $date1 = new DateDTO();
        $date1->libelle = '1er mai';
        $date1->date = '2024-05-01';

        $date2 = new DateDTO();
        $date2->libelle = '8er mai';
        $date2->date = '2024-05-08';

        $date3 = new DateDTO();
        $date3->libelle = 'Noël';
        $date3->date = '2024-12-25';

        $dates = [$date1, $date2, $date3];

        $liveComponent = $this->createLiveComponent(
            name: OtherPublicHolidays::class,
            data: ['otherPublicHolidays' => $dates],
        );

        $this->assertInstanceOf(OtherPublicHolidays::class, $liveComponent->component());
        $component = $liveComponent->component();
        $this->assertCount(3, $component->otherPublicHolidays);
        $mainHTMLNode = $liveComponent->render()->crawler()->filter(self::MAIN_HTML_NODE);
        $this->assertNotEmpty($mainHTMLNode);

        $listItems = $mainHTMLNode->filter('.list-item');
        $this->assertCount(3, $listItems);
        $this->assertStringContainsString('mercredi 1 mai 2024 1er mai', $listItems->eq(0)->text());
        $this->assertStringContainsString('mercredi 8 mai 2024 8er mai', $listItems->eq(1)->text());
        $this->assertStringContainsString('mercredi 25 décembre 2024 Noël', $listItems->eq(2)->text());
    }
}
