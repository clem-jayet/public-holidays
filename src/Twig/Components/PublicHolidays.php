<?php

namespace App\Twig\Components;

use App\DTO\DateDTO;
use App\Enum\Zone;
use App\Interface\PublicHolidaysQueryInterface;
use Symfony\Component\Clock\Clock;
use Symfony\Component\Clock\ClockAwareTrait;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveArg;
use Symfony\UX\LiveComponent\Attribute\LiveListener;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent]
class PublicHolidays
{
    use ClockAwareTrait;
    use DefaultActionTrait;

    public ?DateDTO $nextPublicHoliday = null;

    public int $year;

    public ?string $error = null;

    /** @var array<int, DateDTO> */
    public array $otherPublicHolidays = [];

    public function __construct(private PublicHolidaysQueryInterface $publicHolidaysQuery)
    {
        $this->year = (int) (new Clock())->now()->format('Y');
    }

    public function mount(): void
    {
        $this->updatePublicHolidays(Zone::DEFAULT->value, $this->year);
    }

    #[LiveListener('settings:saved')]
    public function updatePublicHolidays(
        #[LiveArg]
        string $selectedZone,
        #[LiveArg]
        int $selectedYear,
    ): void {
        try {
            $publicHolidays = $this->publicHolidaysQuery->remainingPublicHolidays($selectedZone, $selectedYear);

            $this->year = $selectedYear;
            $this->nextPublicHoliday = $publicHolidays->nextPublicHoliday;
            $this->otherPublicHolidays = $publicHolidays->otherPublicHolidays;
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
        }
    }
}
