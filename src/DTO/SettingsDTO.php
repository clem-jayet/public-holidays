<?php

namespace App\DTO;

use App\Enum\Zone;
use Symfony\Component\Validator\Constraints as Assert;

class SettingsDTO
{
    public function __construct(
        #[Assert\NotNull]
        #[Assert\Choice(callback: [Zone::class, 'values'])]
        public string $zone,
        #[Assert\NotNull]
        #[Assert\GreaterThanOrEqual(2024)]
        #[Assert\Type(type: 'integer')]
        public int $year,
    ) {
    }
}
