<?php

namespace App\Twig\Components;

use App\DTO\DateDTO;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent()]
class NextPublicHoliday
{
    use DefaultActionTrait;

    #[LiveProp(updateFromParent: true)]
    public ?DateDTO $nextPublicHoliday = null;
}
