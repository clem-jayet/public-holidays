<?php

namespace App\Tests\Unit\Twig\Components;

use App\DTO\DateDTO;
use App\DTO\DatesDTO;
use App\Interface\PublicHolidaysQueryInterface;
use App\Twig\Components\PublicHolidays;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class PublicHolidaysTest extends TestCase
{
    private PublicHolidaysQueryInterface&MockObject $publicHolidaysQueryMock;
    private PublicHolidays $publicHoliday;

    protected function setUp(): void
    {
        $this->publicHolidaysQueryMock = $this->createMock(PublicHolidaysQueryInterface::class);
        $this->publicHoliday = new PublicHolidays($this->publicHolidaysQueryMock);
    }

    public function testYearIsSetOnCreation(): void
    {
        $this->assertIsNumeric($this->publicHoliday->year);
    }

    public function testComponentMounted(): void
    {
        $datesDTO = new DatesDTO();
        $datesDTO->nextPublicHoliday = new DateDTO();
        $datesDTO->otherPublicHolidays = [new DateDTO(), new DateDTO()];

        $this->publicHolidaysQueryMock->expects($this->once())
            ->method('remainingPublicHolidays')
            ->with('metropole', '2024')
            ->willReturn($datesDTO);

        $this->publicHoliday->mount();

        $this->assertNotNull($this->publicHoliday->nextPublicHoliday);
        $this->assertInstanceOf(DateDTO::class, $this->publicHoliday->nextPublicHoliday);
        $this->assertNotNull($this->publicHoliday->otherPublicHolidays);
        $this->assertCount(2, $this->publicHoliday->otherPublicHolidays);
    }

    public function testUpdateShouldObtainRemainingPublicHolidays(): void
    {
        $datesDTO = new DatesDTO();
        $datesDTO->nextPublicHoliday = new DateDTO();
        $datesDTO->otherPublicHolidays = [new DateDTO(), new DateDTO()];

        $this->publicHolidaysQueryMock->expects($this->once())
            ->method('remainingPublicHolidays')
            ->with('metropole', 2024)
            ->willReturn($datesDTO);

        $this->publicHoliday->updatePublicHolidays('metropole', 2024);

        $this->assertNotNull($this->publicHoliday->nextPublicHoliday);
        $this->assertCount(2, $this->publicHoliday->otherPublicHolidays);
        $this->assertNull($this->publicHoliday->error);
    }

    public function testRemainingPublicHolidaysThrowException(): void
    {
        $this->publicHolidaysQueryMock->expects($this->once())
            ->method('remainingPublicHolidays')
            ->willThrowException(new \Exception());

        $this->publicHoliday->updatePublicHolidays('unknown-zone', 2024);

        $this->assertNull($this->publicHoliday->nextPublicHoliday);
        $this->assertEmpty($this->publicHoliday->otherPublicHolidays);
        $this->assertNotNull($this->publicHoliday->error);
    }
}
