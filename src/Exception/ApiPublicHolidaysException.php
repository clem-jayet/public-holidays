<?php

namespace App\Exception;

final class ApiPublicHolidaysException extends \Exception
{
    public function __construct(string $errorMessage)
    {
        parent::__construct('Api results an error: '.$errorMessage);
    }
}
