<?php

namespace App\Tests\Unit\HttpRequest;

use App\DTO\SettingsDTO;
use App\Enum\Zone;
use App\Exception\ApiPublicHolidaysException;
use App\HttpRequest\PublicHolidaysApi;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class PublicHolidaysApiTest extends TestCase
{
    private HttpClientInterface&MockObject $httpClientMock;
    private ResponseInterface&MockObject $httpResponseMock;
    private transportException&MockObject $transportExceptionMock;

    protected function setUp(): void
    {
        $this->httpClientMock = $this->createMock(HttpClientInterface::class);
        $this->httpResponseMock = $this->createMock(ResponseInterface::class);
        $this->transportExceptionMock = $this->createMock(TransportException::class);
    }

    #[DataProvider('parametersDataProvider')]
    #[TestDox('Set zone to $zone and year to $year should compute endpoint as $expectedEndpoint')]
    public function testComputeEndpoint(SettingsDTO $settingsDTO, string $expectedEndpoint): void
    {
        $this->httpResponseMock->expects($this->any())
            ->method('getStatusCode')
            ->willReturn(200);
        $this->httpResponseMock->expects($this->any())
            ->method('toArray')
            ->willReturn(['2024-12-25' => 'Jour de noel']);

        $this->httpClientMock->expects($this->any())
            ->method('request')
            ->with('GET', $expectedEndpoint)
            ->willReturn($this->httpResponseMock);

        $publicHolidaysApi = new PublicHolidaysApi($this->httpClientMock);

        $result = $publicHolidaysApi->retrieve($settingsDTO);

        $this->assertNotEmpty($result);
    }

    #[TestDox('An Exception is thrown when api error occures')]
    public function testHttpClientException(): void
    {
        $this->httpClientMock->expects($this->once())
            ->method('request')
            ->willThrowException($this->transportExceptionMock);

        $this->expectException(ApiPublicHolidaysException::class);

        $publicHolidaysApi = new PublicHolidaysApi($this->httpClientMock);
        $publicHolidaysApi->retrieve(new SettingsDTO('metropole', 2024));
    }

    public static function parametersDataProvider(): \Generator
    {
        foreach (Zone::cases() as $zone) {
            yield $zone->value => [new SettingsDTO($zone->value, 2025), 'https://calendrier.api.gouv.fr/jours-feries/'.$zone->value.'/2025.json'];
        }
    }
}
