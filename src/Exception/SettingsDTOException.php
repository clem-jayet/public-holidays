<?php

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

final class SettingsDTOException extends \Exception
{
    public function __construct(ConstraintViolationListInterface $violations)
    {
        $errors = [];
        foreach ($violations as $violation) {
            $errors[] = $violation->getMessage();
        }
        parent::__construct('Settings are not valid: '.implode(' ', $errors));
    }
}
