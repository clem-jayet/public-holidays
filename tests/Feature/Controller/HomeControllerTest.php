<?php

namespace App\Tests\Feature\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    public function testHomeController(): void
    {
        $client = static::createClient();
        $curentYear = date('Y');

        $client->request('GET', '/');

        $client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains(
            'h1',
            sprintf("C'est quand le prochain jour férié en %s ?", $curentYear)
        );
    }
}
