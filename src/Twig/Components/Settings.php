<?php

namespace App\Twig\Components;

use App\Enum\Zone;
use Symfony\Component\Clock\Clock;
use Symfony\Component\Clock\ClockAwareTrait;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\ComponentToolsTrait;
use Symfony\UX\LiveComponent\DefaultActionTrait;
use Symfony\UX\LiveComponent\ValidatableComponentTrait;
use Symfony\UX\TwigComponent\Attribute\ExposeInTemplate;

#[AsLiveComponent]
class Settings
{
    use ComponentToolsTrait;
    use DefaultActionTrait;
    use ValidatableComponentTrait;
    use ClockAwareTrait;

    #[LiveProp(writable: true)]
    #[NotBlank]
    public string $selectedZone = '';

    #[LiveProp(writable: true)]
    #[NotBlank]
    public string $selectedYear;

    public function mount(): void
    {
        $this->selectedZone = Zone::DEFAULT->value;
        $this->selectedYear = (new Clock())->now()->format('Y');
    }

    /** @return array<int, Zone> */
    #[ExposeInTemplate]
    public function getZones(): array
    {
        return Zone::cases();
    }

    /** @return array<int, int> */
    #[ExposeInTemplate]
    public function getYears(): array
    {
        $currentYear = (int) (new Clock())->now()->format('Y');

        return [
            $currentYear,
            $currentYear + 1,
            $currentYear + 2,
            $currentYear + 3,
        ];
    }

    #[LiveAction]
    public function save(): void
    {
        $this->validate();

        $this->emit('settings:saved', [
            'selectedZone' => $this->selectedZone,
            'selectedYear' => $this->selectedYear,
        ]);
    }
}
