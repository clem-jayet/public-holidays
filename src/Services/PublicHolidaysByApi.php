<?php

namespace App\Services;

use App\DTO\DateDTO;
use App\DTO\DatesDTO;
use App\DTO\SettingsDTO;
use App\Exception\SettingsDTOException;
use App\Interface\PublicHolidaysObtainerInterface;
use App\Interface\PublicHolidaysQueryInterface;
use Symfony\Component\Clock\ClockAwareTrait;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PublicHolidaysByApi implements PublicHolidaysQueryInterface
{
    use ClockAwareTrait;

    public function __construct(private PublicHolidaysObtainerInterface $publicHolidaysObtainer, private ValidatorInterface $validator)
    {
    }

    public function remainingPublicHolidays(string $zone, int $year): DatesDTO
    {
        $settingsDTO = new SettingsDTO($zone, $year);
        $errors = $this->validator->validate($settingsDTO);
        if (\count($errors) > 0) {
            throw new SettingsDTOException($errors);
        }

        $jsonData = $this->publicHolidaysObtainer->retrieve($settingsDTO);

        return $this->formatApiResults($jsonData);
    }

    /** @param array<string, string> $jsonData */
    private function formatApiResults(array $jsonData): DatesDTO
    {
        $datesDTO = new DatesDTO();
        $currentDay = $this->now()->format('Y-m-d');

        foreach ($jsonData as $date => $libelle) {
            if ($date < $currentDay) {
                continue;
            }

            $dateDTO = new DateDTO();
            $dateDTO->date = $date;
            $dateDTO->libelle = $libelle;
            if ($datesDTO->nextPublicHoliday === null) {
                $datesDTO->nextPublicHoliday = $dateDTO;
            } else {
                $datesDTO->otherPublicHolidays[] = $dateDTO;
            }
        }

        return $datesDTO;
    }
}
