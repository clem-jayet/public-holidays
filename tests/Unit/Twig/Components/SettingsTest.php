<?php

namespace App\Tests\Unit\Twig\Components;

use App\Enum\Zone;
use App\Twig\Components\Settings;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Clock\Clock;
use Symfony\Component\Clock\MockClock;

final class SettingsTest extends TestCase
{
    private Settings&MockObject $settings;

    protected function setUp(): void
    {
        $this->settings = $this->getMockBuilder(Settings::class)
            ->onlyMethods(['validate', 'emit'])
            ->getMock();

        $this->settings->setClock(new MockClock('2024-04-01 15:20:00'));
    }

    public function testComponentMounted(): void
    {
        $this->settings->mount();
        $this->assertStringContainsString('metropole', $this->settings->selectedZone);
        $this->assertStringContainsString('2024', $this->settings->selectedYear);
    }

    public function testGetZones(): void
    {
        $zones = $this->settings->getZones();
        $this->assertCount(13, $zones);

        foreach ($zones as $zone) {
            $this->assertInstanceOf(Zone::class, $zone);
        }
    }

    public function testGetYears(): void
    {
        $years = $this->settings->getYears();

        $this->assertCount(4, $years);
        $this->assertSame(2024, $years[0]);
        $this->assertSame(2025, $years[1]);
        $this->assertSame(2026, $years[2]);
        $this->assertSame(2027, $years[3]);
    }

    public function testValidateAndEmitOnSave(): void
    {
        $this->settings->expects($this->once())
            ->method('validate');
        $this->settings->expects($this->once())
            ->method('emit')
            ->with(
                'settings:saved',
                [
                    'selectedZone' => 'metropole',
                    'selectedYear' => (new Clock())->now()->format('Y'),
                ]
            );

        $this->settings->mount();
        $this->settings->save();
    }
}
