<?php

namespace App\DTO;

class DatesDTO
{
    public ?DateDTO $nextPublicHoliday = null;

    /** @var array<int, DateDTO> */
    public array $otherPublicHolidays = [];
}
