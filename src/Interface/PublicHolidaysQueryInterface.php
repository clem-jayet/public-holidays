<?php

namespace App\Interface;

use App\DTO\DatesDTO;

interface PublicHolidaysQueryInterface
{
    public function remainingPublicHolidays(string $zone, int $year): DatesDTO;
}
