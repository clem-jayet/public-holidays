<?php

namespace App\Tests\Feature\Twig\Components;

use App\Twig\Components\PublicHolidays;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Clock\Clock;
use Symfony\Component\Clock\ClockAwareTrait;
use Symfony\UX\LiveComponent\Test\InteractsWithLiveComponents;

class PublicHolidaysTest extends KernelTestCase
{
    use ClockAwareTrait;
    use InteractsWithLiveComponents;

    public function testOtherPublicHolidaysCanHaveEmptySubComponents(): void
    {
        $liveComponent = $this->createLiveComponent(
            name: PublicHolidays::class,
        );

        $this->assertInstanceOf(PublicHolidays::class, $liveComponent->component());

        $this->assertStringContainsString(
            sprintf("C'est quand le prochain jour férié en %s ?", (new Clock())->now()->format('Y')),
            $liveComponent->render()->crawler()->filter('h1')->text()
        );

        $this->assertStringContainsString('data-live-name-value="NextPublicHoliday"', $liveComponent->render());
        $this->assertStringContainsString('data-live-name-value="OtherPublicHolidays"', $liveComponent->render());

        $this->assertEmpty($liveComponent->render()->crawler()->filter('#next-public-holiday'));
        $this->assertEmpty($liveComponent->render()->crawler()->filter('#other-public-holidays'));
    }
}
