<?php

namespace App\Twig\Components;

use App\DTO\DateDTO;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\ComponentToolsTrait;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent]
class OtherPublicHolidays
{
    use DefaultActionTrait;
    use ComponentToolsTrait;

    /**
     * @var DateDTO[]
     */
    #[LiveProp(updateFromParent: true)]
    public array $otherPublicHolidays = [];
}
