<?php

namespace App\Tests\Unit\Services;

use App\DTO\SettingsDTO;
use App\Exception\SettingsDTOException;
use App\Interface\PublicHolidaysObtainerInterface;
use App\Services\PublicHolidaysByApi;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Clock\MockClock;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class PublicHolidaysByApiTest extends TestCase
{
    private PublicHolidaysObtainerInterface&MockObject $publicHolidaysObtainer;
    private ValidatorInterface&MockObject $validator;

    protected function setUp(): void
    {
        $this->publicHolidaysObtainer = $this->createMock(PublicHolidaysObtainerInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
    }

    #[TestDox('Public Holidays should have been retrieved and setted on result object')]
    public function testpublicHolidaysByApi(): void
    {
        $this->publicHolidaysObtainer
            ->method('retrieve')
            ->with(new SettingsDTO('metropole', 2024))
            ->willReturn([
                '2024-01-05' => 'Date non retenue',
                '2024-04-01' => 'lundi de pâques',
                '2024-05-01' => 'Premier mai',
                '2024-03-31' => 'Autre date non retenue',
                '2024-12-25' => 'Noël',
            ]);

        $publicHolidaysByApi = new PublicHolidaysByApi($this->publicHolidaysObtainer, $this->validator);
        $publicHolidaysByApi->setClock(new MockClock('2024-04-01 15:20:00'));
        $results = $publicHolidaysByApi->remainingPublicHolidays('metropole', 2024);

        $this->assertNotNull($results->nextPublicHoliday);
        $this->assertSame('2024-04-01', $results->nextPublicHoliday->date);
        $this->assertSame('lundi de pâques', $results->nextPublicHoliday->libelle);
        $this->assertCount(2, $results->otherPublicHolidays);
        $this->assertSame('2024-05-01', $results->otherPublicHolidays[0]->date);
        $this->assertSame('Premier mai', $results->otherPublicHolidays[0]->libelle);
        $this->assertSame('2024-12-25', $results->otherPublicHolidays[1]->date);
        $this->assertSame('Noël', $results->otherPublicHolidays[1]->libelle);
    }

    #[TestDox('Validator should throw an exception when settings are not as expected')]
    public function testSettingsDTOisNotValidException(): void
    {
        $this->validator
            ->expects($this->once())
            ->method('validate')
            ->willReturn(
                new ConstraintViolationList([$this->createMock(ConstraintViolation::class)])
            )
        ;

        $this->expectException(SettingsDTOException::class);
        $publicHolidaysByApi = new PublicHolidaysByApi($this->publicHolidaysObtainer, $this->validator);
        $publicHolidaysByApi->remainingPublicHolidays('', 0);
    }
}
