<?php

namespace App\Tests\Feature\Twig\Components;

use App\DTO\DateDTO;
use App\Twig\Components\NextPublicHoliday;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\UX\LiveComponent\Test\InteractsWithLiveComponents;

class NextPublicHolidayTest extends KernelTestCase
{
    use InteractsWithLiveComponents;

    public const MAIN_HTML_NODE = '#next-public-holiday';

    public function liveComponentCanBeEmpty(): void
    {
        $liveComponent = $this->createLiveComponent(
            name: NextPublicHoliday::class,
            data: [],
        );

        $this->assertInstanceOf(NextPublicHoliday::class, $liveComponent->component());
        $component = $liveComponent->component();
        $this->assertNull($component->nextPublicHoliday);
        $this->assertEmpty($liveComponent->render()->crawler()->filter(self::MAIN_HTML_NODE));
    }

    public function testComponenWithData(): void
    {
        $dto = new DateDTO();
        $dto->libelle = 'Premier mai';
        $dto->date = '2024-05-01';
        $liveComponent = $this->createLiveComponent(
            name: NextPublicHoliday::class,
            data: ['nextPublicHoliday' => $dto],
        );

        $this->assertInstanceOf(NextPublicHoliday::class, $liveComponent->component());
        $component = $liveComponent->component();

        $this->assertInstanceOf(DateDTO::class, $component->nextPublicHoliday);

        $renderedHTML = $liveComponent->render()->crawler()->filter(self::MAIN_HTML_NODE);
        $this->assertNotEmpty($renderedHTML);
        $this->assertStringContainsString('Le mercredi 1 mai 2024 aussi connu sous le nom de Premier mai', $renderedHTML->text());
    }
}
