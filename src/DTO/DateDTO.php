<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class DateDTO
{
    #[Assert\NotBlank]
    public string $date;

    #[Assert\NotBlank]
    public string $libelle;
}
